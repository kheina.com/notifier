import json
import os
import re
from asyncio import Task, ensure_future, get_event_loop
from enum import Enum
from re import compile as re_compile
from secrets import token_bytes
from typing import Optional, Union

import aiohttp
from avrofastapi.serialization import AvroDeserializer, AvroSerializer
from crc16 import crc16xmodem as cksum
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from kh_common import logging
from kh_common.base64 import b64decode, b64encode
from kh_common.caching import ArgsCache
from kh_common.config.credentials import encryption_key
from kh_common.crawling import tagSplit
from kh_common.hashing import Hashable
from kh_common.models.telegram import CallbackQuery, Chat, InlineKeyboardButton, InlineKeyboardMarkup, Message, Update
from kh_common.telegram import Listener as TelegramListener
from kh_common.telegram import QuitParsing
from pydantic import BaseModel


Message.update_forward_refs()
CallbackQuery.update_forward_refs()
Chat.update_forward_refs()

logger = logging.getLogger()
PostMessageRegex = re_compile(r'^(?P<title>[\s\S]+) by (?P<artist>[^\s]+) matched your tags: (?P<tags>(?:(?:[^\s]+, )*(?:[^\s]+)))\nsample, source, artist, remove tag: \/rt(?P<tag_hash>\d{5})$')
CookieRegex = re_compile(r'^\s*(?P<key>[^=]+?)\s*=\s*(?P<value>[^;]+?);?\s*$')
FavKeyRegexTemplate = r'"\/(?:un)?fav\/{post_id}\/?\?key\=(?P<key>[0-9a-f]{{40}})"'


class Website(Enum) :
	FurAffinity: str = 'furaffinity'
	E621: str = 'e621'
	# add more websites BELOW


class ButtonPurpose(Enum) :
	Favorite: str = 'favorite'
	Unfavorite: str = 'unfavorite'
	# add more purposes BELOW


class MarkupButton(BaseModel) :
	"""
	NOTE: the encoded version of this model must be less than 64 bytes
	"""
	website: Website
	purpose: ButtonPurpose
	post: str  # this needs to be agnostic so that we can add new websites in the future


ButtonSerializer: AvroSerializer = AvroSerializer(MarkupButton)
ButtonDeserializer: AvroDeserializer = AvroDeserializer(MarkupButton)


def deleteUser(user) :
	userPath = f'notifier/data/{user}.json'
	if os.path.isfile(userPath) :
		try :
			os.remove(userPath)
		except :
			logger.exception('unable to delete user profile.')
			return False
		return True
	return False


class Listener(TelegramListener, Hashable) :

	def __init__(self) :
		self.key: AESGCM = AESGCM(b64decode(encryption_key['key']))
		Hashable.__init__(self)
		TelegramListener.__init__(
			self,
			allow_chats = True,
			bot_name = 'kheinawatchbot',
			responses = {
				'/start': '\n'.join([
					'This bot sends you alerts any time a new image is indexed by kheina.com. you can control what you get alerted for via tags.',
					'Try running /help for some assistance using this bot.',
				]),
				'/help': '\n'.join([
					'<b>general info:</b>',
					'    tags: you will receive an alert any time an image with your given tags is indexed. ex: <code>/add snow_leopard</code>',
					'    tag groups: you can submit groups of tags which will send you an alert only when all tags in a given group are matched. ex: <code>/add snow, leopard</code>',
					'    negated tags: you can also match on the absense of a tag. you can do this by preceding the tag with a "!" character. ex: <code>/add snow_leopard, !explicit</code>',
					'    blocked tags: submissions containing any blocked tags or tag groups will never be sent to you. ex: <code>/block gore</code>',
					'',
					'<b>helpful commands:</b>',
					'    /commands: display all available commands and what they do',
					'    /advanced: display a few different advanced features this bot is capable of',
				]),
				'/commands': '\n    '.join([
					'<b>commands:</b>',
					'/commands: shows this menu',
					'/help: explains how the tagging system this bot offers works',
					'/advanced: display a few different advanced features this bot is capable of',
					'/tags: displays the tags you are currently notified for',
					'/blocked: displays the tags that you currently have blocked',
					'/add: adds a single tag or tag group to be notified for',
					'/remove: halts notifications for a single tag or tag group',
					'/block: adds a single tag or tag group to your block list',
					'/unblock: removes a single tag or tag group from your block list',
					"/watch: reply to an upload message with this command to add that post's artist to your tag list",
					'/pause: pauses notifications, can be disabled later by re-running this command',
					'/stop: stops this service and deletes your profile',
				]),
				'/advanced': '\n    '.join([
					'<b>slash modifiers:</b>',
					'you can add a "/" and a character at the end of your tag to make it exclusive to a specific function',
					'_/a: only match the artist (even if the artist exists as a tag). ex: <code>/add snow_leopard/a</code>',
					'_/r: only match the rating flag. ex: <code>/addbl explicit/r</code>',
				]),
				'/poot': '💨',
				'/bless': '🙏',
			},
			commands = {
				'/tags': self.sendTagList,
				'/blocked': self.sendBlockList,
				'/add': self.addTag,
				'/remove': self.removeTag,
				'/addbl': self.addBlockedTag,
				'/block': self.addBlockedTag,
				'/removebl': self.removeBlockedTag,
				'/unblock': self.removeBlockedTag,
				'/rb': self.rb,
				'/rt': self.rt,
				'/stop': self.stop,
				'/watch': self.watch,
				'/pause': self.pause,
				'/unpause': self.pause,
				'/login': self.login,
				'/logout': self.logout,
			},
		)


	async def login(self, user, is_chat, text, message) :
		if is_chat :
			return

		if not text :
			await self.sendMessage(user, 'in order to favorite posts, you must log in. to log in, run the /login command followed by your browser cookie for furaffinity. it should look something like the following:')
			await self.sendMessage(user, '<code>/login b=01234567-89ab-cdef-0123-456789abcdef; a=01234567-89ab-cdef-0123-456789abcdef; sz=1200x1000; cc=1; n=0123456789</code>')
			await self.sendMessage(user, 'in order to store your credentials securely, your login data will be encrypted and sent as a file back to this chat. it will <b>not</b> be stored locally by the bot. use /logout to delete any login info')

		if text :
			cookies = list(filter(None, map(CookieRegex.match, text.split(';'))))
			data = {
				m.group('key'): m.group('value')
				for m in cookies
			}
			login = json.dumps(data).encode()
			nonce = token_bytes(12)
			encrypted = self.key.encrypt(nonce, login, None)
			document = b64encode(encrypted) + b'.' + b64encode(nonce)
			form = aiohttp.FormData({
				'chat_id': str(user),
				'caption': 'this is your encrypted login info',
			})
			form.add_field('document', document, filename='encrypted-login')

			async with aiohttp.request('POST', f'https://api.telegram.org/bot{self._telegram_access_token}/sendDocument', data=form) as r :
				if r.status >= 400 :
					ensure_future(self.sendMessage(user, 'something went wrong when I tried to send your encrypted login data'))
					return

				user_data = self.loadUserData(user, { })
				body = await r.json()
				user_data['login_file_id'] = body['result']['document']['file_id']
				self.writeUserData(user, user_data)

			ensure_future(self.sendMessage(user, 'you have successfully logged in! I highly recommend you delete this message, just in case', reply_to=message.message_id))


	async def logout(self, user, is_chat) :
		if is_chat :
			return

		user_data = self.loadUserData(user)
		if 'login_file_id' in user_data :
			del user_data['login_file_id']
			self.writeUserData(user, user_data)

			ensure_future(self.sendMessage(user, "okay, I've deleted the file id of your encrypted login info."))

		else :
			ensure_future(self.sendMessage(user, 'I do not have any login data stored for you'))


	async def handleUnknownCommand(self, chat, command, **kwargs) :
		if command.startswith('/rt') :
			await self.rt(chat=chat, text=command[3:])
			return

		if command.startswith('/rb') :
			ensure_future(self.rb(chat=chat, text=command[3:]))
			return

		await TelegramListener.handleUnknownCommand(self, chat, command=command, **kwargs)


	async def parseUpdateWithoutMessage(self, update: Update) :
		if update.callback_query :
			return await self.parseCallbackQuery(update.callback_query)

		await TelegramListener.parseUpdateWithoutMessage(self, update)


	async def answerCallbackQuery(self, callback_query: CallbackQuery, text: str, new_markup: Optional[InlineKeyboardMarkup] = None) :
		async with aiohttp.request('POST', f'https://api.telegram.org/bot{self._telegram_access_token}/answerCallbackQuery', data={
			'callback_query_id': callback_query.id,
			'text': text,
		}, raise_for_status=True) :
			pass

		if not new_markup :
			return

		async with aiohttp.request('POST', f'https://api.telegram.org/bot{self._telegram_access_token}/editMessageReplyMarkup', data={
			'chat_id': callback_query.message.chat.id,
			'message_id': callback_query.message.message_id,
			'reply_markup': new_markup.json(encoder=lambda x : x, exclude_unset=True),
		}, raise_for_status=True) :
			pass


	async def parseCallbackQuery(self, callback_query: CallbackQuery) :
		# start getting credentials immediately
		credentials: Task[dict] = ensure_future(self.getFurAffinityCredentials(callback_query.from_user.id))
		data: MarkupButton = ButtonDeserializer(b64decode(callback_query.data))

		if data.purpose == ButtonPurpose.Favorite :
			fav_key_regex = FavKeyRegexTemplate.format(post_id=data.post)
			credentials: dict = await credentials

			if not credentials :
				return await self.answerCallbackQuery(callback_query, 'requires login! use /login in my DMs')

			fav_key: str
			async with aiohttp.request('GET', f'https://www.furaffinity.net/view/{data.post}/', cookies=credentials) as r :
				if r.status >= 400 :
					return await self.answerCallbackQuery(callback_query, 'failed to favorite post')

				match = re.search(fav_key_regex, await r.text())
				if not match :
					return await self.answerCallbackQuery(callback_query, 'failed to favorite post')

				fav_key = match.group('key')

			async with aiohttp.request('GET', f'https://www.furaffinity.net/fav/{data.post}/?key={fav_key}', cookies=credentials) as r :
				if r.status >= 400 :
					return await self.answerCallbackQuery(callback_query, 'failed to favorite post')

				else :
					return await self.answerCallbackQuery(callback_query, '❤️', new_markup=InlineKeyboardMarkup(inline_keyboard=[[
						InlineKeyboardButton(text='Unfavorite Post', callback_data=b64encode(ButtonSerializer(MarkupButton(website=data.website, purpose=ButtonPurpose.Unfavorite, post=data.post))).decode()),
					]]))

		if data.purpose == ButtonPurpose.Unfavorite :
			fav_key_regex = FavKeyRegexTemplate.format(post_id=data.post)
			credentials: dict = await credentials

			fav_key: str
			async with aiohttp.request('GET', f'https://www.furaffinity.net/view/{data.post}/', cookies=credentials) as r :
				if r.status >= 400 :
					return await self.answerCallbackQuery(callback_query, 'failed to unfavorite post')

				match = re.search(fav_key_regex, await r.text())
				if not match :
					return await self.answerCallbackQuery(callback_query, 'failed to unfavorite post')

				fav_key = match.group('key')

			async with aiohttp.request('GET', f'https://www.furaffinity.net/unfav/{data.post}/?key={fav_key}', cookies=credentials) as r :
				if r.status >= 400 :
					return await self.answerCallbackQuery(callback_query, 'failed to unfavorite post')

				else :
					return await self.answerCallbackQuery(callback_query, '💔', new_markup=InlineKeyboardMarkup(inline_keyboard=[[
						InlineKeyboardButton(text='Favorite Post', callback_data=b64encode(ButtonSerializer(MarkupButton(website=data.website, purpose=ButtonPurpose.Favorite, post=data.post))).decode()),
					]]))


	@ArgsCache(60)
	async def getFurAffinityCredentials(self, user: int) :
		file_id: str = self.loadUserData(user, { }).get('login_file_id')

		if not file_id :
			return None

		document: bytes
		async with aiohttp.request('POST', f'https://api.telegram.org/bot{self._telegram_access_token}/getFile', data={ 'file_id': file_id }, raise_for_status=True) as r :
			result = await r.json()

			if not result['ok'] :
				return None

			async with aiohttp.request('GET', f'https://api.telegram.org/file/bot{self._telegram_access_token}/{result["result"]["file_path"]}', raise_for_status=True) as rf :
				document = await rf.read()

		encrypted, nonce = document.split(b'.')
		login: bytes = self.key.decrypt(b64decode(nonce), b64decode(encrypted), None)
		return json.loads(login)


	def loadUserData(self, chat, default=None) :
		try :
			with open(f'notifier/data/{chat}.json') as data :
				return json.load(data)

		except FileNotFoundError :
			if default is not None :
				return default
			ensure_future(self.sendMessage(chat, "it doesn't look like you have a profile yet, try adding some tags."))

		raise QuitParsing('no profile found.')


	def writeUserData(self, user, data) :
		with open(f'notifier/data/{user}.json', 'w') as userfile :
			return json.dump(data, userfile)


	@ArgsCache(60*60)
	def tagHash(tag) :
		return str(cksum(str(tag).encode())).rjust(5, '0')


	async def stop(self, user, chat=None, text=None, message=None) :
		if text and text.lower() == 'delete' :
			if deleteUser(chat) :
				ensure_future(self.sendMessage(chat, 'done, your profile has been deleted.'))
			else :
				ensure_future(self.sendMessage(chat, "hmm, it looks like something weird happened and I wasn't able to delete your profile.\nplease message @darius for assistance."))
		elif text :
			ensure_future(self.sendMessage(chat, "I couldn't understand your request, run <code>/stop delete</code> to delete your profile."))
		else :
			ensure_future(self.sendMessage(chat, 'WARNING!\nthis will delete your whole profile!\n\nrun <code>/stop delete</code> if you are sure.'))


	async def pause(self, user, chat=None, text=None, message=None) :
		userData = self.loadUserData(chat, { })
		userData['paused'] = not userData.get('paused')
		self.writeUserData(chat, userData)
		if userData['paused'] :
			ensure_future(self.sendMessage(chat, 'the service has been paused. run /pause again to resume.\n\n(you may receive a few more messages before the command is processed)'))
		else :
			ensure_future(self.sendMessage(chat, 'the service has been resumed.'))


	async def rt(self, user=None, chat=None, text=None, message=None) :
		userData = self.loadUserData(chat)
		try :
			index, tags = next(filter(lambda x : Listener.tagHash(tuple(x[1])) == text, enumerate(userData['allowed'])))

		except StopIteration :
			ensure_future(self.sendMessage(chat, "sorry, I couldn't find that tag in your tag list"))

		else :
			del userData['allowed'][index]
			self.writeUserData(chat, userData)
			ensure_future(self.sendMessage(chat, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your tag list'))


	async def rb(self, user=None, chat=None, text=None, message=None) :
		userData = self.loadUserData(chat)
		try :
			index, tags = next(filter(lambda x : Listener.tagHash(tuple(x[1])) == text, enumerate(userData['blocked'])))

		except StopIteration :
			ensure_future(self.sendMessage(chat, "sorry, I couldn't find that tag in your blocked tags"))

		else :
			del userData['blocked'][index]
			self.writeUserData(chat, userData)
			ensure_future(self.sendMessage(chat, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your blocked tags'))


	async def sendTagList(self, user, chat=None, text=None, message=None) :
		userData = self.loadUserData(chat)
		if 'allowed' in userData :
			ensure_future(self.sendMessage(chat, 'here are the tags you are currently notified for:\n' + '\n'.join([
				f'/rt{Listener.tagHash(tuple(tags))} ' + ', '.join(tags)
				for tags in sorted(userData['allowed'])
			])))
		else :
			ensure_future(self.sendMessage(chat, "it doesn't look like you are notified for any tags yet, try adding some"))


	async def sendBlockList(self, user, chat=None, text=None, message=None) :
		userData = self.loadUserData(chat)
		if 'blocked' in userData :
			ensure_future(self.sendMessage(chat, 'here are the tags you currently have blocked:\n' + '\n'.join([
				f'/rb{Listener.tagHash(tuple(tags))} ' + ', '.join(tags)
				for tags in sorted(userData['blocked'])
			])))
		else :
			ensure_future(self.sendMessage(chat, "it doesn't look like you have a block list yet, try blocking some tags"))


	async def addTag(self, user, chat=None, text=None, message=None) :
		if not text.strip() :
			ensure_future(self.sendMessage(chat, "I can't listen for empty tags"))
			return

		groups = tuple(map(lambda x : tuple(tagSplit(x)), text.split(';')))
		userData = self.loadUserData(chat, { })
		userData['allowed'] = set(map(tuple, userData.get('allowed', [])))

		for tags in groups :
			userData['allowed'].add(tags)

		userData['allowed'] = list(userData['allowed'])
		self.writeUserData(chat, userData)
		if len(groups) > 1 :
			ensure_future(self.sendMessage(chat, 'okay, the following tags have been added to your tag list:\n    ' + '\n    '.join([
				f'/rt{Listener.tagHash(tuple(tags))} ' + ', '.join(tags)
				for tags in groups
			])))

		else :
			ensure_future(self.sendMessage(chat, 'okay, <i>' + ', '.join(tags) + '</i> has been added to your tag list'))


	async def removeTag(self, chat=None, text=None) :
		userData = self.loadUserData(chat)
		tags = list(tagSplit(text))
		try : userData['allowed'].remove(tags)
		except :
			ensure_future(self.sendMessage(chat, "sorry, it doesn't look like <i>" + ', '.join(tags) + '</i> is in your tag list'))
		else :
			self.writeUserData(chat, userData)
			ensure_future(self.sendMessage(chat, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your tag list'))


	async def addBlockedTag(self, chat=None, text=None) :
		if not text.strip() :
			ensure_future(self.sendMessage(chat, "I can't block empty tags"))
			return

		groups = tuple(map(lambda x : tuple(tagSplit(x)), text.split(';')))
		userData = self.loadUserData(chat, { })
		userData['blocked'] = set(map(tuple, userData.get('blocked', [])))

		for tags in groups :
			userData['blocked'].add(tags)

		userData['blocked'] = list(userData['blocked'])
		self.writeUserData(chat, userData)
		if len(groups) > 1 :
			ensure_future(self.sendMessage(chat, 'okay, the following tags have been added to your block list:\n    ' + '\n    '.join([
				f'/rb{Listener.tagHash(tuple(tags))} ' + ', '.join(tags)
				for tags in groups
			])))
		else :
			ensure_future(self.sendMessage(chat, 'okay, <i>' + ', '.join(tags) + '</i> has been added to your block list'))


	async def removeBlockedTag(self, chat=None, text=None) :
		userData = self.loadUserData(chat)
		tags = list(tagSplit(text))
		try : userData['blocked'].remove(tags)
		except :
			ensure_future(self.sendMessage(chat, "sorry, it doesn't look like <i>" + ', '.join(tags) + '</i> is in your block list'))
		else :
			self.writeUserData(chat, userData)
			ensure_future(self.sendMessage(chat, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your block list'))


	async def watch(self, chat=None, message=None) :
		message = message.get('reply_to_message')

		if not message :
			return ensure_future(self.sendMessage(chat, "sorry, I didn't understand that. to use the watch command, reply to an upload message with /watch, and I'll add that artist to your tag list."))

		if message['from']['id'] != self._telegram_bot_id :
			return ensure_future(self.sendMessage(chat, "sorry, I didn't understand that. to use the watch command, you must reply to an upload message that I sent."))

		parsed = PostMessageRegex.match(message['text'])
		tag = (f'{parsed.group("artist").lower()}/a',)

		userData = self.loadUserData(chat)
		userData['allowed'] = set(map(tuple, userData.get('allowed', [])))

		userData['allowed'].add(tag)

		userData['allowed'] = list(userData['allowed'])
		self.writeUserData(chat, userData)

		ensure_future(self.sendMessage(chat, 'okay, <i>' + tag[0] + '</i> has been added to your tag list'))


if __name__ == '__main__' :
	loop = get_event_loop()
	try :
		listener = Listener()
		loop.run_until_complete(listener.run())

	finally :
		pass
