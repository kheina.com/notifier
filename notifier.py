import asyncio
import os
import sys
import time
from html import escape
from re import compile as re_compile

import ujson as json
from kh_common.base64 import b64encode
from kh_common.caching import SimpleCache
from kh_common.config.credentials import telegram
from kh_common.crawling import normalizeTag
from kh_common.logging import Logger, getLogger
from kh_common.message_queue import Receiver
from kh_common.models.telegram import InlineKeyboardButton, InlineKeyboardMarkup

from listener import ButtonPurpose, ButtonSerializer, Listener, MarkupButton, Website


logger: Logger = getLogger('notifier', disable=['pika'])
listener: Listener = Listener()


def isint(s) :
	try : return int(s)
	except : return None


class Notifier(Receiver) :

	TitleTags = re_compile('('+'|'.join([
		'ych', 'reminder', 'raffle', 'adopt',
		# 'sale', 'important', 'open', 'closed', 'contest',
		'[a-z]+',
		# '\w+',
	])+')')

	def __init__(self, looptime=5, ttl=float('inf'), dedupe=5) :
		self.looptime = looptime
		self.TTL = time.time() + ttl
		self._dedupe = set(range(dedupe))

		Receiver.__init__(self)

		self._telegram_access_token = telegram['telegram_access_token']
		self._telegram_bot_id = telegram['telegram_bot_id']
		self.logger = logger = getLogger('notifier', disable=['pika'])


	async def filter(self, hash) :
		if hash in self._dedupe :
			return False

		# set should rotate the items on its own
		self._dedupe.pop()
		self._dedupe.add(hash)
		return True


	async def alert(self, item) :
		userMap = Notifier.getTagTree()

		# add a bunch of extra tags we can pull from the item fields
		tags = item['tags'] + [
			normalizeTag(item['artist']),
			item['artist'].lower()+'/a',
			item['rating'].lower(),
			item['rating'].lower()+'/r',
		]
		title = item['title']
		if title :
			# nothing returned from title tags should need tag normalization
			tags += Notifier.TitleTags.findall(title.lower())

		tags = TreeTraverser(tags)
		tags(userMap)

		# if no one's going to receive the message, just return early
		if not tags.recipients :
			return

		# make sure we haven't sent alerts for this image recently
		if not await self.filter(item['sha1']) :
			return

		message_p1 = (f'<b>{escape(title)}</b>' if title else 'new upload') + ' by ' + item['artist'] + ' matched your tags: <i>'
		links = [f'<a href="{item["url"]}">source</a>']

		if item.get('imageurl') :
			links = [f'<a href="{item["imageurl"]}">sample</a>'] + links

		if item.get('artisturl') :
			links.append(f'<a href="{item["artisturl"]}">artist</a>')

		message_p2 = ', '.join(links)

		count = 0
		for recipient, path in tags.recipients.items() :
			markup: InlineKeyboardMarkup = None
			if userMap.user_data[recipient].get('logged_in') :
				markup = InlineKeyboardMarkup(
					inline_keyboard=[[
						InlineKeyboardButton(text='Favorite Post', callback_data=b64encode(ButtonSerializer(MarkupButton(website=Website(item['website']), purpose=ButtonPurpose.Favorite, post=item['id']))).decode()),
					]],
				)

			count += await listener.sendMessage(recipient, message_p1 + ', '.join(path) + '</i>\n' + message_p2 + f', remove tag: /rt{Listener.tagHash(tuple(path))}', markup=markup)

		logger.info({
			'count': count,
			'allowed': len(tags.recipients),
			'blocked': len(tags.blocked),
		})


	async def notify(self, item) :
		try :
			await self.alert(item)

		except :
			logger.exception({
				'message': 'notification failed.',
				'item': item,
			})


	@SimpleCache(60)
	def getTagTree() :
		userData = Notifier.pullUserData()
		processed = TreeRoot()

		# eventually, we should be sorting each tag group to maximize users with the same groups for efficiency

		for user, prefs in userData.items() :
			# skip consumption if paused
			if prefs.get('paused') :
				continue

			# for list_type in TreeRoot.list_types :
			# allowed first
			for tags in prefs.get('allowed', []) :
				Notifier.addTagsToTree(processed.allowed, tags, user)

			# now blocked
			for tags in prefs.get('blocked', []) :
				Notifier.addTagsToTree(processed.blocked, tags, user)

			processed.user_data[int(user)] = {
				'logged_in': 'login_file_id' in prefs,
			}

		# traverse the tree to see if it can be simplified anywhere
		"""
		for list_type, tree in processed.items() :

			for root, content in tree.items() :
				content.keys()
		"""

		logger.info(f'consumed preferences for {len(userData)} users.')
		return processed


	def pullUserData() :
		return {
			file[:-5]: json.load(open('notifier/data/'+file))
			for file in os.listdir('notifier/data')
			if file.endswith('.json') and isint(file[:-5])
		}


	def addTagsToTree(tree, tags, user) :
		for tag in tags :
			match = True
			if tag.startswith('!') :
				match = False
				tag = tag[1:]

			if match :
				if not tree.match :
					tree.match = { }
				tree = tree.match

			else :
				if not tree.nomatch :
					tree.nomatch = { }
				tree = tree.nomatch

			if tag not in tree :
				tree[tag] = TreeNode()
			tree = tree[tag]

		if not tree.users :
			tree.users = set()
		tree.users.add(int(user))


	async def run(self) :

		while True :
			starttime = time.time()
			metadata = self.recv(kwargs.get('forcelist'))

			for item in metadata :
				asyncio.ensure_future(self.notify(item))

			endtime = time.time()
			elapsedtime = endtime - starttime
			sleepfor = self.looptime - elapsedtime
			if sleepfor + endtime > self.TTL :
				return
			await asyncio.sleep(sleepfor)


	def recv(self, forcelist=False) :
		# just let it fail if it's not json serialized
		if forcelist :
			return list(map(json.loads, self._recv()))
		else :
			return map(json.loads, self._recv())


class TreeRoot :
	list_types = ['allowed', 'blocked']
	def __init__(self) :
		self.allowed = TreeNode()
		self.blocked = TreeNode()
		self.user_data = { }

	def dict(self) :
		return { 'allowed': self.allowed.dict(), 'blocked': self.blocked.dict() }


class TreeNode :

	def __init__(self) :
		self.users = None
		self.match = None
		self.nomatch = None

	def dict(self) :
		return {
			'users': list(self.users) if self.users else None,
			'match': { k: v.dict() for k, v in self.match.items() } if self.match else None,
			'nomatch': { k: v.dict() for k, v in self.nomatch.items() } if self.nomatch else None,
		}


class TreeTraverser :

	def __init__(self, tags) :
		self.recipients = { }
		self.blocked = set()
		self.tags = set(tags)

	def __call__(self, tree: TreeRoot) :
		self.blupdate(tree.blocked)
		self.update(tree.allowed)

	# assume the block list is fully-formed
	def update(self, tree) :
		# eliminate as many keys immediately as possible, then iterate over them
		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._update(tree.match[key], [key])

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._update(tree.nomatch[key], ['!' + key])

	def _update(self, tree, path) :
		# we can either remove the blocked members here, or do it afterward
		# an analysis should be done on which method is faster
		# self.recipients.update({ k: path for k in tree.users - self.blocked })
		if tree.users :
			for user in tree.users - self.blocked - self.recipients.keys() :
				self.recipients[user] = path

		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._update(tree.match[key], path + [key])

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._update(tree.nomatch[key], path + ['!' + key])

	# run this first
	def blupdate(self, tree) :
		# eliminate as many keys immediately as possible, then iterate over them
		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._blupdate(tree.match[key])

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._blupdate(tree.nomatch[key])

	def _blupdate(self, tree) :
		if tree.users :
			self.blocked.update(tree.users)

		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._blupdate(tree.match[key])

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._blupdate(tree.nomatch[key])


if __name__ == '__main__' :
	from ast import literal_eval
	kwargs = { k: literal_eval(v) for k, v in (arg.split('=') for arg in sys.argv[1:]) }

	notifier = Notifier(**kwargs)
	loop = asyncio.get_event_loop()
	loop.run_until_complete(notifier.run())
	time.sleep(1)
	loop.close()
